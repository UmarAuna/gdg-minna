package com.gdg.gdgminna;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
         drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content, new GDGHOME()).commit();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.rate) {
            String url="https://www.google.com";
            Intent r= new Intent(Intent.ACTION_VIEW);
            r.setData(Uri.parse(url));
            startActivity(r);
            return true;
        }
        if(id == R.id.share){
            Intent share = new Intent (Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_SUBJECT,"GDG Minna");
            share.putExtra(Intent.EXTRA_TEXT,"Download GDG Minna App to know more about Google Developers Group Minna Community");
            startActivity(share);
            return true;
        }
        if(id == R.id.about){
            Intent a = new Intent(MainActivity.this,ABOUT.class);
            startActivity(a);
            return true;
        }

        if(id == R.id.feedback){
            Intent f = new Intent(MainActivity.this,FEEDBACK.class);
            startActivity(f);
            return true;
        }

        if (id == R.id.teams){
            Intent t = new Intent(MainActivity.this,TEAMS.class);
            startActivity(t);
            return true;
        }

        if(id == R.id.website){
            Intent w = new Intent(getApplicationContext(),WEBSITE.class);
            startActivity(new Intent(w));
            return true;
        }

        if(id == R.id.disclaimer){
            Intent d = new Intent(MainActivity.this,DISCLAIMER.class);
            startActivity(d);
            return true;
        }

        if(id == R.id.location){
            Intent l =  new Intent(MainActivity.this,Location.class);
            startActivity(l);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.gdghome) {
            fragment = new GDGHOME();
            ft.replace(R.id.content, fragment).commit();
        } else if (id == R.id.wtminna) {
            fragment = new WTM();
            ft.replace(R.id.content, fragment).commit();
        } else if (id == R.id.blog) {
            fragment = new BLOG();
            ft.replace(R.id.content, fragment).commit();
        } else if (id == R.id.pluspage) {
            fragment = new PPAGE();
            ft.replace(R.id.content, fragment).commit();
        } else if (id == R.id.organizers) {
            fragment = new ORGANIZERS();
            ft.replace(R.id.content, fragment).commit();
        } else if (id == R.id.contactus) {
            fragment = new CONTACT_US();
            ft.replace(R.id.content, fragment).commit();
        }else if (id == R.id.sponsors) {
            fragment = new SPONSORS();
            ft.replace(R.id.content, fragment).commit();
        }else if (id == R.id.register) {
            fragment = new REGISTER();
            ft.replace(R.id.content, fragment).commit();
        }else if (id == R.id.forum) {
            fragment = new FORUM();
            ft.replace(R.id.content, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

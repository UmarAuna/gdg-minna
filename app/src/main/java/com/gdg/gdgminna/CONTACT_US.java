package com.gdg.gdgminna;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class CONTACT_US extends Fragment {
    private View rootView;


    public CONTACT_US() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_contact__us, container, false);
        CardView group = (CardView) rootView.findViewById(R.id.groups);
        CardView plus = (CardView) rootView.findViewById(R.id.plus);
        CardView facebook = (CardView) rootView.findViewById(R.id.facebook);
        CardView twitter = (CardView) rootView.findViewById(R.id.twitter);
        CardView instagram = (CardView) rootView.findViewById(R.id.instagram);
        CardView youtube = (CardView) rootView.findViewById(R.id.youtube);
        CardView developers = (CardView) rootView.findViewById(R.id.developers);
        CardView women = (CardView) rootView.findViewById(R.id.women);
        group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="http://groups.google.com/d/forum/gdg-minna";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://plus.google.com/b/113224920428716041640/+GdgminnaBlogspot/posts";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://www.facebook.com/GDGMinna";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://twitter.com/GDGMinna";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://www.instagram.com/gdg_minna/";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://www.youtube.com/channel/UCTYCALPn-1LCHm6IP6jIdLw";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        developers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://developers.google.com/groups/chapter/113224920428716041640/";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        women.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url="https://www.womentechmakers.com/";
                Intent i= new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });





        return rootView;
    }


}
